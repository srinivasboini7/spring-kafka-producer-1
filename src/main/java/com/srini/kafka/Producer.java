package com.srini.kafka;

import com.opencsv.CSVReader;
import com.opencsv.exceptions.CsvException;
import jakarta.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class Producer {

    private final KafkaTemplate<String,String> kafkaTemplate ;
    private final ResourceLoader resourceLoader;

    public void send(){
        Resource resource = resourceLoader.getResource("classpath:bank-sheet1.csv") ;


        try (CSVReader reader = new CSVReader(new FileReader(resource.getFile()))) {
            List<String[]> records = reader.readAll() ;

            Map<String, String> map = records.stream().limit(500).collect(Collectors.toMap(e-> RandomStringUtils.randomAlphanumeric(1,30)+e[0], Arrays::toString, (a, b)-> a) );

            System.out.println(map);
            System.out.println(map.size());

            map.entrySet()
                    .stream()
                    .map(e -> new ProducerRecord("test-topic-1", e.getKey(),e.getValue()))
                    .forEach(kafkaTemplate::send);


        } catch (IOException | CsvException e) {
            throw  new RuntimeException(e.getMessage()) ;
        }
    }
}
